/**
 * Calculates the total price of added items.
 * 
 * @author Piyush Kochhar
 *
 */
public class CashRegister implements TotalPriceCalculatable, DiscountPrintable {
  
  private double price;
  private double quantity;
  private double[] discounts = {10, 20};
  private double result;
  private static double total = 0;
  private String value = " ";
  
  public double getPrice() {
    return price;
  }
  
  public void setPrice(double price) {
    this.price = price;
  }
  
  public double getQuantity() {
    return quantity;
  }
  
  public void setQuantity(double quantity) {
    this.quantity = quantity;
  }
  
  public double[] getDiscounts() {
    return discounts;
  }
  
  public void setDiscounts(double[] discounts) {
    this.discounts = discounts;
  }
  
  public double getResult() {
    return result;
  }
  
  public void setResult(double result) {
    this.result = result;
  }
  
  public static double getTotal() {
    return total;
  }
  
  public static void setTotal(double total) {
    CashRegister.total = total;
  }
  
  public String getValue() {
    return value;
  }
  
  public void setValue(String value) {
    this.value = value;
  }
  
  /**
   * Displays total price for checkout.
   * 
   * @return Total price
   */
  public String toString() {
    return "Total : " + total;
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public void display(double[] discounts) {
    for (int i = 0; i < discounts.length; i++) {
      System.out.println((i + 1) + ". " + discounts[i] + "% Off.");
    }
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public double priceWithDiscount(double price, int discount) {
    price = price - (price * discount / 100);
    return price;
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public double priceWithoutDiscount(double price, double quantity) {
    return price * quantity;
  }
  
  @Override
  public double totalPrice(double price, double quantity) {
    result = priceWithoutDiscount(price, quantity);
    result = priceWithDiscount(result, (int) discounts[Integer.parseInt(value) - 1]);
    total = total + result;
    return result;
  }
  
}
