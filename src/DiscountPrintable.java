/**
 * Displays discount options available.
 * 
 * @author Piyush Kochhar
 *
 */
public interface DiscountPrintable {
  
  /**
   * Displays discount options available.
   * 
   * @param discounts discount options
   */
  void display(double[] discounts);
  
}
