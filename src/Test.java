import java.util.Scanner;

public class Test extends CashRegister {
  /**
   * Tester Class.
   */
  public static void main(String[] args) {
    
    CashRegister cr = new CashRegister();
    
    Scanner sc = new Scanner(System.in);
    
    while (cr.getValue().charAt(0) != 'n') {
      
      System.out.println("Enter the price");
      cr.setPrice(sc.nextDouble());
      
      System.out.println("Enter the quantity");
      cr.setQuantity(sc.nextDouble());
      
      cr.display(cr.getDiscounts());
      
      System.out.println("Choose your option or press q to quit.");
      cr.setValue(sc.nextLine());
      
      cr.setValue(sc.nextLine());
      
      System.out.println(cr.getValue());
      
      if (cr.getValue().charAt(0) == 'q') {
        break;
      }
      cr.totalPrice(cr.getPrice(), cr.getQuantity());
      
      System.out.println(cr);
      
      System.out.println("Do you want to calculate more : press y for Yes n for No?");
      cr.setValue(sc.nextLine());
    }
    
    
  }
  
}
