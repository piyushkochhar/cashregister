import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Test;

public class CashRegisterTest {
  
  @Test
  public void testPriceWithDiscount() {
    try {
      CashRegister cr = new CashRegister();
      assertEquals(90, cr.priceWithDiscount(100, 10), 0.001);
    } catch (Exception e) {
      fail("Expected an Integer or Float");
    }
  }
  
  @Test
  public void testPriceWithoutDiscount() {
    CashRegister cr = new CashRegister();
    assertEquals(10, cr.priceWithoutDiscount(5, 2), 0.001);
  }
  
  @Test
  public void testTotalPrice() {
    CashRegister cr = new CashRegister();
    cr.setValue("1");
    assertEquals(5.4, cr.totalPrice(3, 2), 0.001);
  }
  
}
