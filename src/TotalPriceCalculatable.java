/**
 * Methods for calculating price.
 * 
 * @author Piyush Kochhar
 *
 */
public interface TotalPriceCalculatable {
  /**
   * Calculates total price without discount.
   * 
   * @param price price of the item
   * @param quantity total number of item
   * @return total price without discount
   */
  double priceWithoutDiscount(double price, double quantity);
  
  /**
   * Calculates total price with discount.
   * 
   * @param price price of the item
   * @param discount discount option of the item
   * @return total price with discount
   */
  double priceWithDiscount(double price, int discount);
  
  // Only for internal working
  double totalPrice(double price, double quantity);
  
}
